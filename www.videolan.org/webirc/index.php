<?php
   $title = "VideoLAN IRC support channel";
   $menu = array( "vlc", "" );
   $body_color = "red";
   $new_design = true;
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<div class="container">
    <h1 class="bigtitle">VideoLAN IRC support channel</h1>

    <div class="row">
        <h1>Using your web browser</h1>
        <p>
            Just pick a pseudo and connect without password.
        </p>
        <a class="btn btn-default" target="_new" href="https://kiwiirc.com/nextclient/#ircs://irc.libera.chat/#videolan">Connect using WebIRC</a>
        <hr/>
        <h1>Using your own IRC client</h1>
        <p>
            Our channel is on <a target="_new" href="http://libera.chat">Libera.chat</a> Network<br />
            Server: <code>irc.libera.chat</code></br>
            Channel: <code>#videolan</code></br>
            URI: <a target="_new" href="ircs://irc.libera.chat:6697">ircs://irc.libera.chat:6697</a></br>
        </p>
    </div>
</div>
<?php footer('$Id$'); ?>
