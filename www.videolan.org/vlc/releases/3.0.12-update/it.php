            <center><h1 class='bigtitle' style="padding-bottom: 3px;"><em>Problema nell'aggiornamento automatico</em> in VLC <b>3.0.12</b> e <b>3.0.13</b></h1>
            <div style="padding-top: 0px; padding-bottom: 10px; color: grey;">Un bug nell'aggiornamento automatico impedisce agli utenti Windows di scaricare gli aggiornamenti automatici.</div>
            </center>
        <div class="container">

    <center><h2>Questo articolo riguarda esclusivamente gli utenti Windows</h2></center>

<h3>Riassunto:</h3>
<ul>
<li>- le versioni 3.0.12 e 3.0.13 <b>non</b> possono scaricare gli aggiornamenti automatici, si <b>deve intervenire</b> manualmente</li>
<li>- le versioni 3.0.11 e precedenti posson aggiornare automaticamente alla versione 3.0.14</li>
</ul>
<br/>

<h3>Descrizione:</h3>
Questo avviso riguarda gli utenti di VLC con versione 3.0.12 e 3.0.13.<br/>
A causa di un errore introdotto nel codice per gli aggiornamenti automatici, gli aggiornamenti verrano scaricati e validati correttamente ma non potranno essere installati. Ci scusiamo per il disguido.<br/><br/>

<h3>Istruzioni:</h3>
Per poter aggiornare a VLC 3.0.14, bisogna scaricare VLC dal sito <a href="https://www.videolan.org/vlc">https://www.videolan.org/vlc</a> e installare l'applicazione manualmente.<br/>
Si possono trovare informazioni dettagliate <a href="https://docs.videolan.me/vlc-user/3.0/en/gettingstarted/setup/windows.html">qui</a>.<br/><br/>

Se l'aggiornamento e' gia' stato scaricato, si puo' lanciare manualmente accedendo all'esplora risorse (tasto Windows + E, o semplicamente cliccando sull'icona) ed usare <em>%TEMP%</em> come destinazione nella barra degli indirizzi.<br/>
Il programma di installazione sara' in quella cartella col nome «vlc-3.0.14-win32.exe» o «vlc-3.0.14-win64.exe» rispettivamente, in base alla version di Windows in uso (32bit o 64bit).<br/>
<br/>
<?php image("screenshots/3.0.12-update.jpg" , "3.0.12 update screen", "center-block img-responsive"); ?>
<br/>
<br/>

<h3>Spiegazione post-mortem:</h3>
Il 10 maggio2021, l'organizzazion VideoLAN ha rilasciato VLC 3.0.13 ed abilitato gli aggiornamenti automatici.<br/>
Questo processo dovrebbe essere relativamente semplice, con una notifica che appare per avvisare che e' disponibilie un aggiornamento - basterebbe cliccare su Download e far partire l'installazione e basta.<br/>
Tuttavia e sfortunatamente per questo aggiornamento in particolare bisogna ancora effettuare un paio di passaggi manuali per completare l'operazione.<br/>
Il problema e' stato introdotto nella versione 3.0.12, ma non e' stato trovato fino a quando non e' stata rilasciata la versione 3.0.13.<br/>
L'aggiornamento 3.0.14 risolve questo problema e dobbiamo assicurarci che tutti gli utenti effettuino l'aggiornamento a questa versione per poter garantire che gli aggiornamenti futuri vengano installati correttamente.<br/>

<br/>
<a href="https://code.videolan.org/videolan/vlc-3.0/-/commit/83d8e7efaa4f7dc23b07c47c59431e1f6df57da5">
Il commit che ha introdotto il bug</a><br/>
<a href="https://code.videolan.org/videolan/vlc-3.0/-/commit/d456994213b98933664bd6aee2e8f09d5dea5628">Il commit che ha risolto il bug per le versioni future</a><br/>


