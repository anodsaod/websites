            <center><h1 class='bigtitle' style="padding-bottom: 3px;"><em>Problema com o atualizador automático</em> no VLC <b>3.0.12</b> e <b>3.0.13</b></h1>
            <div style="padding-top: 0px; padding-bottom: 10px; color: grey;">Um defeito com o atualizador automático impede uma actualização do VLC no Windows.</div>
            </center>

        <div class="container">

    <center><h2>Isso é relevante apenas para utilizadores no Windows</h2></center>

<h3>Sumário:</h3>
Esse aviso é relevante para utilizadores do VLC 3.0.13 e VLC 2.0.12. Devido a um defeito introduzido no códico do atualizador automático, a atualização é baixada, verificada mas não é installada. Isto é mau e nós pedirmos desculpas por isso.<br/>
<br/>

<h3>Instruções:</h3>
Para atualizar o VLC para a versão 3.0.14, é necessário the navigar para <a href="https://www.videolan.org/vlc">https://www.videolan.org/vlc</a> e baixar e instalar o VLC manualmente.<br/><br/>

Se você já executou a atualização automática que baixou a nóva versão do VLC, é possivel de iniciar a instalação manualmente: Abré o explorador do Windows (Tecla Windows + E ou clique no ícone do explorador) e navegue para <em>%TEMP%</em>. Você deve encontrar o programa the instalação aqui, nomeado vlc-3.0.14-win32.exe ou vlc-3.0.14-win64.exe respectivamente a versão de Windows de 32bit ou 64bit.
<br/>
<br/>
<?php image("screenshots/3.0.12-update.jpg" , "3.0.12 update screen", "center-block img-responsive"); ?>
<br/>
<br/>

<h3>Explicação post mortem:</h3>
Em 10 de maio de 2021 a organização VideoLAN publicou a versão 3.0.13 do VLC e ativou a atualização automatica.<br/>Isso normalmente é simples, uma janela iria aparecer com a informação sobre a nóva versão, você clique baixar e instalar e é isso.<br/>Contudo e infelizmente, para essa atualização particular, alguns passos addiçionais serão necessários.<br/>O problema é introduzido na versão 3.0.12, mas não se tornou óbvio até publicar a versão 3.0.13.<br/>Enquanto o problema é resolvido na versão 3.0.14, não é possivel fiar-se nisso para usários que já instalaram a versão 3.0.12.
<br/>

<br/>
<a href="https://code.videolan.org/videolan/vlc-3.0/-/commit/83d8e7efaa4f7dc23b07c47c59431e1f6df57da5">
A mudança que introduzio o problema</a><br/>
<a href="https://code.videolan.org/videolan/vlc-3.0/-/commit/d456994213b98933664bd6aee2e8f09d5dea5628">A mudança que resolveu o problema</a><br/>

