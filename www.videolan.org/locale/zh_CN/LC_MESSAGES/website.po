# Simplified Chinese translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Dean Lee <xslidian@gmail.com>, 2012-2017
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2017-12-07 18:52+0100\n"
"Last-Translator: Dian Li <xslidian@gmail.com>, 2017\n"
"Language-Team: Chinese (China) (http://www.transifex.com/yaron/vlc-trans/"
"language/zh_CN/)\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: include/header.php:289
msgid "a project and a"
msgstr "既是一个项目组，也是一家"

#: include/header.php:289
msgid "non-profit organization"
msgstr "非盈利组织"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "合作伙伴"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "团队 &amp; 组织"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "咨询服务 &amp; 合作伙伴"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "活动"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "法律"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "新闻中心"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "联系我们"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "下载"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "功能"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "自定义"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "获取素材"

#: include/menus.php:51
msgid "Projects"
msgstr "项目"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "所有项目"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "贡献"

#: include/menus.php:77
msgid "Getting started"
msgstr "入门"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "捐助"

#: include/menus.php:79
msgid "Report a bug"
msgstr "报告 bug"

#: include/menus.php:83
msgid "Support"
msgstr "支持"

#: include/footer.php:33
msgid "Skins"
msgstr "皮肤"

#: include/footer.php:34
msgid "Extensions"
msgstr "扩展"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "截图"

#: include/footer.php:61
msgid "Community"
msgstr "社区"

#: include/footer.php:64
msgid "Forums"
msgstr "论坛"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "邮件列表"

#: include/footer.php:66
msgid "FAQ"
msgstr "常见问题"

#: include/footer.php:67
msgid "Donate money"
msgstr "捐助资金"

#: include/footer.php:68
msgid "Donate time"
msgstr "捐献时间"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "项目及组织"

#: include/footer.php:76
msgid "Team"
msgstr "团队"

#: include/footer.php:80
msgid "Mirrors"
msgstr "镜像列表"

#: include/footer.php:83
msgid "Security center"
msgstr "安全中心"

#: include/footer.php:84
msgid "Get Involved"
msgstr "加入"

#: include/footer.php:85
msgid "News"
msgstr "新闻"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "下载 VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "其他系统"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "次下载"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC 是一款自由、开源的跨平台多媒体播放器及框架，可播放大多数多媒体文件，以及 "
"DVD、音频 CD、VCD 及各类流媒体协议。"

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC 是一款免费、自由、开源的跨平台多媒体播放器及框架，可播放大多数多媒体文"
"件，以及各类流媒体协议。"

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "VLC：官方网站 - 全平台的自由多媒体解决方案！"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "VideoLAN 的其他项目"

#: index.php:30
msgid "For Everyone"
msgstr "适合所有用户"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr "VLC 是一款强大的媒体播放器，能播放当今大多数媒体及视频格式。"

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr "VideoLAN Movie Creator 是一款非线性视频创作编辑软件。"

#: index.php:62
msgid "For Professionals"
msgstr "适合专业人士"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr "DVBlast 是一款简单但强大的 MPEG-2/TS 反复用及串流应用程序。"

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr "multicat 是一组为简单高效处理组播串流及 TS 而设计的工具。"

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr "x264 是用于将视频流编码为 H.264/MPEG-4 AVC 格式的自由应用程序。"

#: index.php:104
msgid "For Developers"
msgstr "适合开发人员"

#: index.php:140
msgid "View All Projects"
msgstr "查看所有项目"

#: index.php:144
msgid "Help us out!"
msgstr "提供帮助！"

#: index.php:148
msgid "donate"
msgstr "捐助"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN 是一家非盈利组织。"

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
" 我们的全部开销均由用户捐助承担。如果您喜欢使用 VideoLAN 的某款产品，请捐助支"
"持我们。"

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "了解更多"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN 是一款开源软件。"

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr "这意味着如果您有能力，并希望改进我们的某款产品，我们欢迎您的贡献"

#: index.php:187
msgid "Spread the Word"
msgstr "帮助宣传"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"我们认为 VideoLAN 拥有市面上最棒的视频软件，其价位也最为合理：免费。如果您同"
"意我们的观点，请帮忙宣传我们的软件。"

#: index.php:215
msgid "News &amp; Updates"
msgstr "新闻 &amp; 动态"

#: index.php:218
msgid "More News"
msgstr "更多新闻"

#: index.php:222
msgid "Development Blogs"
msgstr "开发博客"

#: index.php:251
msgid "Social media"
msgstr "社交媒体"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr "官方下载：VLC media player，最棒的开源播放器"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "获取 VLC for"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "简单、快速、强大"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "能播放任何内容"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "文件、光盘、摄像头、设备及流媒体"

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "可播放大多数格式，无需安装编解码器包"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "可在所有平台运行"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "完全免费"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "无间谍软件，无广告，无跟踪用户的行为"

#: vlc/index.php:47
msgid "learn more"
msgstr "了解更多"

#: vlc/index.php:66
msgid "Add"
msgstr "添加"

#: vlc/index.php:66
msgid "skins"
msgstr "外观"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "设计外观可采用"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "VLC 外观编辑器"

#: vlc/index.php:72
msgid "Install"
msgstr "安装"

#: vlc/index.php:72
msgid "extensions"
msgstr "扩展"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "查看所有截图"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "VLC media player 官方下载"

#: vlc/index.php:146
msgid "Sources"
msgstr "源码"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "您也可以直接获取"

#: vlc/index.php:148
msgid "source code"
msgstr "源码"

#~ msgid "A project and a"
#~ msgstr "既是一个项目组，也是一家"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr "由志愿者组成，开发并推广自由、开源的多媒体解决方案。"

#~ msgid "why?"
#~ msgstr "为何?"

#~ msgid "Home"
#~ msgstr "主页"

#~ msgid "Support center"
#~ msgstr "支持中心"

#~ msgid "Dev' Zone"
#~ msgstr "开发者空间"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "简单、快速、强大的媒体播放器。"

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "可播放任何介质：文件、光盘、摄像头、设备及流媒体。"

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "可播放大多数格式，而无需安装编解码器包："

#~ msgid "Runs on all platforms:"
#~ msgstr "可在所有平台运行："

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr "完全免费，无间谍软件，无广告，无跟踪用户的行为。"

#~ msgid "Can do media conversion and streaming."
#~ msgstr "可对媒体进行转换与串流。"

#~ msgid "Discover all features"
#~ msgstr "发现全部功能"

#~ msgid ""
#~ "VLC is a free and open source cross-platform multimedia player and "
#~ "framework that plays most multimedia files as well as DVD, Audio CD, VCD, "
#~ "and various streaming protocols."
#~ msgstr ""
#~ "VLC 是一款免费、自由、开源的跨平台多媒体播放器及框架，可播放大多数多媒体文"
#~ "件，DVD、音频 CD、VCD 以及各类流媒体协议。"

#~ msgid "DONATE"
#~ msgstr "捐助"

#~ msgid "Other Systems and Versions"
#~ msgstr "其他系统及版本"

#~ msgid "Other OS"
#~ msgstr "其他平台"
